//functions used to generate 'tables.html.' 

var linedata;

d3.json("dale.json", function (d) {
    linedata = d;
});

//number of metrical elements in the entire play
function getTotalElements() {
    var x = 0;
    for (var i in linedata) {
        x += linedata[i].MetricalElements;
    }
    return x;
}

//number of verses (lines) in the entire play
function getTotalVerses() {
    var x = 0;
    for (var i in linedata) {
        x += 1;
    }
    return x;
};

//get list of all characters in the play
function returnChars() {
    var chars = [];
    for (var i in linedata) {
        if (!chars.includes(linedata[i].Character)) {
            chars.push(linedata[i].Character);
        }
    }
    return chars;
}

//return array of the total number of metrical elements spoken by each character
function getElementsByChar() {
    var elementsByChar = [];
    var chars = returnChars();
    for (var c in chars) {
        var numME = 0;
        for (var i in linedata) {
            if (linedata[i].Character == chars[c]) {
                numME += linedata[i].MetricalElements;
            }
        }
        elementsByChar[chars[c]] = numME;
    }
    return elementsByChar;
};

//return array of the total number of verses spoken by each character
function getVersesByChar() {
    var versesByChar = [];
    var chars = returnChars();
    for (var c in chars) {
        var numVerses = 0;
        for (var i in linedata) {
            if (linedata[i].Character == chars[c]) {
                numVerses += 1;
            }
        }
        versesByChar[chars[c]] = numVerses;
    }
    return versesByChar;
};

//return list of all meters in play (abbreviated forms)
function getMeters() {
    var meters = [];
    for (var i in linedata) {
        if (!meters.includes(linedata[i].Meter)) {
            meters.push(linedata[i].Meter);
        }
    }
    return meters;
}

//return list of all meters in play (full names)
function getMeterNames() {
    var meternames = [];
    for (var i in linedata) {
        if (!meternames.includes(linedata[i].Name)) {
            meternames.push(linedata[i].Name);
        }
    }
    return meternames;
}

//return list of all types of meters in play
function getTypes() {
    var types = [];
    for (var i in linedata) {
        if (!types.includes(linedata[i].Type) && linedata[i].Type != null) {
            types.push(linedata[i].Type);
        }
    }
    return types;
}

function getElementsByType() {
    var elementsByType= [];
    var types = getTypes();
    for (var t in types) {
        var numME = 0;
        for (var i in linedata) {
            if (linedata[i].Type == types[t]) {
                numME += linedata[i].MetricalElements;
            }
        }
        elementsByType[types[t]] = numME;
    }
    return elementsByType;
}

function getVersesByType() {
    var versesByType = [];
    var types = getTypes();
    for (var t in types) {
        var numVerses = 0;
        for (var i in linedata) {
            if (linedata[i].Type == types[t]) {
                numVerses += 1;
            }
        }
        versesByType[types[t]] = numVerses;
    }
    return versesByType;
}


//get total number of metrical elements spoken in each meter
function getElementsByMeter() {
    var elementsByMeter = [];
    var meters = getMeters();
    for (var m in meters) {
        var numME = 0;
        for (var i in linedata) {
            if (linedata[i].Meter == meters[m]) {
                numME += linedata[i].MetricalElements;
            }
        }
        elementsByMeter[meters[m]] = numME;
    }
    return elementsByMeter;
};

//get total number of verses spoken in each meter
function getVersesByMeter() {
    var versesByMeter = [];
    var meters = getMeters();
    for (var m in meters) {
        var numVerses = 0;
        for (var i in linedata) {
            if (linedata[i].Meter == meters[m]) {
                numVerses += 1;
            }
        }
        versesByMeter[meters[m]] = numVerses;
    }
    return versesByMeter;
};

//return list of all parts of the play
function returnParts() {
    var partsofplay = [];
    for (var i in linedata) {
        if (!partsofplay.includes(linedata[i].Part)) {
            partsofplay.push(linedata[i].Part);
        }
    }
    return partsofplay;
}

//get array of total number of metrical elements spoken in each part of the play
function getElementsByPart() {
    var elementsByPart = [];
    var parts = returnParts();
    for (var p in parts) {
        var numME = 0;
        for (var i in linedata) {
            if (linedata[i].Part == parts[p]) {
                numME += linedata[i].MetricalElements;
            }
        }
        elementsByPart[parts[p]] = numME;
    }
    return elementsByPart;
};

//get array of total number of verses spoken in each part of the play
function getVersesByPart() {
    var versesByPart = [];
    var parts = returnParts();
    for (var p in parts) {
        var numVerses = 0;
        for (var i in linedata) {
            if (linedata[i].Part == parts[p]) {
                numVerses += 1;
            }
        }
        versesByPart[parts[p]] = numVerses;
    }
    return versesByPart;
};

//return array of total number of metrical elements spoken by each gender
function getElementsByGender() {
    var elementsByGender = [];
    femaleElements = 0;
    maleElements = 0;
    for (var i in linedata) {
        if (linedata[i].Gender == "Female") {
            femaleElements += linedata[i].MetricalElements;
        } else {
            maleElements += linedata[i].MetricalElements;
        }
    }
    elementsByGender["female"] = femaleElements;
    elementsByGender["male"] = maleElements;
    return elementsByGender;
}

//return array of the total number of verses spoken by each gender
function getVersesByGender() {
    var versesByGender = [];
    femaleVerses = 0;
    maleVerses = 0;
    for (var i in linedata) {
        if (linedata[i].Gender == "Female") {
            femaleVerses += 1;
        } else {
            maleVerses += 1;
        }
    }
    versesByGender["female"] = femaleVerses;
    versesByGender["male"] = maleVerses;
    return versesByGender;
}

//return array of the total number of metrical elements spoken by characters of each status (slave/free)
function getElementsByStatus() {
    var elementsByStatus = [];
    slaveElements = 0;
    freeElements = 0;
    for (var i in linedata) {
        if (linedata[i].Status == "Free") {
            freeElements += linedata[i].MetricalElements;
        } else {
            slaveElements += linedata[i].MetricalElements;
        }
    }
    elementsByStatus["free"] = freeElements;
    elementsByStatus["slave"] = slaveElements;
    return elementsByStatus;
};

//return array of the total number of verses spoken by characters of each status
function getVersesByStatus() {
    var versesByStatus = [];
    slaveVerses = 0;
    freeVerses = 0;
    for (var i in linedata) {
        if (linedata[i].Status == "Free") {
            freeVerses += 1;
        } else {
            slaveVerses += 1;
        }
    }
    versesByStatus["free"] = freeVerses;
    versesByStatus["slave"] = slaveVerses;
    return versesByStatus;
};

//return array of the total number of metrical elements spoken by human characters and by god characters
function getElementsByMortality() {
    var elementsByMortality = [];
    humanElements = 0;
    godElements = 0;
    for (var i in linedata) {
        if (linedata[i].Mortality == "Human") {
            humanElements += linedata[i].MetricalElements;
        } else {
            godElements += linedata[i].MetricalElements;
        }
    }
    elementsByMortality["human"] = humanElements;
    elementsByMortality["god"] = godElements;
    return elementsByMortality;
};

//return array of the total number of verses spoken by human and god characters
function getVersesByMortality() {
    var versesByMortality = [];
    humanVerses = 0;
    godVerses = 0;
    for (var i in linedata) {
        if (linedata[i].Mortality == "Human") {
            humanVerses += 1;
        } else {
            godVerses += 1;
        }
    }
    versesByMortality["human"] = humanVerses;
    versesByMortality["god"] = godVerses;
    return versesByMortality;
};

//get array of the number of metrical elements spoken by each character in each meter
function getElementsByMetersByChar() {
    var elementsByMetersByChar = [];
    var chars = returnChars();
    var meters = getMeters();
    for (var c in chars) {
        elementsByMetersByChar[chars[c]] = [];
    }
    for (var c in chars) {
        for (var m in meters) {
            var elems = 0;
            for (var i in linedata) {
                if (linedata[i].Character == chars[c] && linedata[i].Meter == meters[m]) {
                    elems += linedata[i].MetricalElements;
                }
            }
            elementsByMetersByChar[chars[c]][meters[m]] = elems;
        }
    }
    return elementsByMetersByChar;
};

//get array of the number of verses spoken by each character in each meter
function getVersesByMetersByChar() {
    var versesByMetersByChar = [];
    var chars = returnChars();
    var meters = getMeters();
    for (var c in chars) {
        versesByMetersByChar[chars[c]] = [];
    }
    for (var c in chars) {
        for (var m in meters) {
            var verses = 0;
            for (var i in linedata) {
                if (linedata[i].Type != null) {
                    if (linedata[i].Character == chars[c] && linedata[i].Type == meters[m]) {
                        verses += 1;
                    }
                } else {
                    if (linedata[i].Character == chars[c] && linedata[i].Meter == meters[m]) {
                        verses += 1;
                    }
                }
            }
            versesByMetersByChar[chars[c]][meters[m]] = verses;
        }
    }
    return versesByMetersByChar;
}

//return the number of times one inputted meter follows the other in the play
function followedBy(first, second) {
    var count = 0;
    for (var i = 0; i < linedata.length - 1; i++) {
        if ((linedata[i + 1].Name == second && linedata[i].Name == first && second != first) || (linedata[i + 1].Type== second && linedata[i].Type == first && second != first)){
            count += 1;
        }
    }
    return count;
};

//return a list of the line numbers of the places where one meter comes after the other
function returnLocations(first, second) {
    var loc = [];
    for (var i = 0; i < linedata.length - 1; i++) {
        if ((linedata[i + 1].Name == second && linedata[i].Name == first && second != first) || (linedata[i + 1].Type== second && linedata[i].Type == first && second != first)) {
            loc.push(linedata[i].Barnes);
        }
    }
    return loc;
}

//get the total number of metrical elements spoken in each type of play segement, episode or stasimon
function getElementsByPartType() {
    var elementsByPartType = [];
    episodeElements = 0;
    stasimonElements = 0;
    for (var i in linedata) {
        if (linedata[i].Part.includes("Episode")) {
            episodeElements += linedata[i].MetricalElements;
        } else if (linedata[i].Part.includes("Stasimon")) {
            stasimonElements += linedata[i].MetricalElements;
        }
    }
    elementsByPartType["episodes"] = episodeElements;
    elementsByPartType["stasima"] = stasimonElements;
    return elementsByPartType;
}

//get the total number of verses spoken in each type of play segment
function getVersesByPartType() {
    var versesByPartType = [];
    episodeVerses = 0;
    stasimonVerses = 0;
    for (var i in linedata) {
        if (linedata[i].Part.includes("Episode")) {
            episodeVerses += 1;
        } else if (linedata[i].Part.includes("Stasimon")) {
            stasimonVerses += 1;
        }
    }
    versesByPartType["episodes"] = episodeVerses;
    versesByPartType["stasima"] = stasimonVerses;
    return versesByPartType;
}

//return an array of the meters spoken by each character
function getMetersByChar() {
    var metersByChar = {};
    var chars = returnChars();
    for (var c in chars) {
        metersByChar[chars[c]] = [];
    }
    for (var c in chars) {
        for (var i in linedata) {
            if (linedata[i].Character == chars[c]) {
                if (!metersByChar[chars[c]].includes(linedata[i].Name)) {
                    metersByChar[chars[c]].push(linedata[i].Name);
                }
            }
        }
    }
    return metersByChar;
}

//return an array of the meters spoken by each gender
function getMetersByGender() {
    var metersByGender = { "Female": [], "Male": [] };
    for (var i in linedata) {
        if (linedata[i].Gender == "Female") {
            if (!metersByGender["Female"].includes(linedata[i].Name)) {
                metersByGender["Female"].push(linedata[i].Name);
            }
        } else {
            if (!metersByGender["Male"].includes(linedata[i].Name)) {
                metersByGender["Male"].push(linedata[i].Name);
            }
        }
    }
    return metersByGender;
}

//return an array of the meters spoken by each status
function getMetersByStatus() {
    var metersByStatus = { "Free": [], "Slave": [] };
    for (var i in linedata) {
        if (linedata[i].Status == "Free") {
            if (!metersByStatus["Free"].includes(linedata[i].Name)) {
                metersByStatus["Free"].push(linedata[i].Name);
            }
        } else {
            if (!metersByStatus["Slave"].includes(linedata[i].Name)) {
                metersByStatus["Slave"].push(linedata[i].Name);
            }
        }
    }
    return metersByStatus;
}

//return an array of the meters spoken by humans and the meters spoken by gods
function getMetersByMortality() {
    var metersByMortality = { "Human": [], "God": [] };
    for (var i in linedata) {
        if (linedata[i].Mortality == "Human") {
            if (!metersByMortality["Human"].includes(linedata[i].Meter)) {
                metersByMortality["Human"].push(linedata[i].Meter);
            }
        } else {
            if (!metersByMortality["God"].includes(linedata[i].Meter)) {
                metersByMortality["God"].push(linedata[i].Meter);
            }
        }
    }
    return metersByMortality;
}

//return array of total number of metrical elements spoken in lyric vs nonlyric verse
function getElementsByLyric() {
    var elementsByLyric = [];
    lyricElements = 0;
    nonlyricElements = 0;
    for (var i in linedata) {
        if (linedata[i].Lyric == "lyric") {
            lyricElements += linedata[i].MetricalElements;
        } else if (linedata[i].Lyric == "non-lyric"){
            nonlyricElements += linedata[i].MetricalElements;
        }
    }
    elementsByLyric["lyric"] = lyricElements;
    elementsByLyric["nonlyric"] = nonlyricElements;
    return elementsByLyric;
}

//return array of the total number of verses spoken in lyric vs nonlyric verse
function getVersesByLyric() {
    var versesByLyric = [];
    lyricVerses = 0;
    nonlyricVerses = 0;
    for (var i in linedata) {
        if (linedata[i].Lyric == "lyric") {
            lyricVerses += 1;
        } else if (linedata[i].Lyric == "non-lyric"){
            nonlyricVerses += 1;
        }
    }
    versesByLyric["lyric"] = lyricVerses;
    versesByLyric["nonlyric"] = nonlyricVerses;
    return versesByLyric;
}

//consolidate the lists of meters by attribute into one array
function getMetersByAll() {
    var m = getMetersByMortality();
    var s = getMetersByStatus();
    var g = getMetersByGender();
    var c = getMetersByChar();
    var metersByAll = [];
    metersByAll.push(m);
    metersByAll.push(s);
    metersByAll.push(g);
    metersByAll.push(c);
    return metersByAll;
}

